/**
 * @authors:Zhihao Yang, Jiapeng Lu
 * @file lab09.cpp
 * @brief Lab 9; and exploration of dynamic programming approaches to the
 *        Fibonacci sequence.
 */
#include <cstdlib>
#include <iostream>
#include <map>

// Function prototypes
int topDownFib(int);
int bottomUpFib(int);

// Global variable accessible in both main and topDownFib
std::map<int, int> m;

/**
 * Entry point of this application.
 *
 * @return EXIT_SUCCESS upon successful completion.
 */
int main() {
  std::cout << "This program computes the nth Fibonacci number." << std::endl;
  std::cout << "To end this program, enter a negative value for n." << std::endl;
  std::cout << std::endl;
  int n{0};
  std::cout << "n = ";
  std::cin >> n;
  while (n >= 0) {
    std::cout << "Top-down approach: fib(" << n << ") = " << topDownFib(n) << std::endl;
    std::cout << "Bottom-up approach: fib(" << n << ") = " << bottomUpFib(n) << std::endl;
    std::cout << std::endl;
    std::cout << "n = ";
    std::cin >> n;
  }
  std::cout << std::endl << "Program has ended successfully. Good bye..." << std::endl;
}

/**
 * Dynamic, top-down approach to finding the nth Fibonacci number.
 *
 * @param n the number (index) in the sequence
 * @return The nth Fibonacci number is returned.
 */
int topDownFib(int n) { 
  std::map<int, int> m;
  
  if (m.find(n) == m.end())
  {
	  if (n == 1 || n == 2)
	  {
		  m[n] = 1;
	  }
	  else
	  {
		  m[n] = topDownFib(n-1) + topDownFib(n-2);
	  }
  }
  
  return m[n];
}

/**
 * Dynamic, bottom-up approach to finding the nth Fibonacci number.
 *
 * @param n the number (index) in the sequence
 * @return The nth Fibonacci number is returned.
 */
int bottomUpFib(int n) { 
  int m[n + 1];
  
  m[1] = m[2] = 1;
  
  for (int i = 3; i < n + 1; i++)
  {
	  m[i] = m[i-1] + m[i-2];
  }
  
  return m[n];
}
